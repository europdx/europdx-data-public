cancer_study_identifier: ircc_crc_feb2018
genetic_alteration_type: MRNA_EXPRESSION
datatype: Z-SCORE
stable_id: rna_seq_mrna_capture_Zscores
show_profile_in_analysis_tab: true
profile_name: mRNA expression (ILMN-Z-scores)
profile_description: Expression levels (Log2, Illumina HT-12 v4 microarray)
data_filename: data_expression_file_Zscore.txt
